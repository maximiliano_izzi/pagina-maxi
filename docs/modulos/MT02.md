---
hide:
    - toc
---

# MT 02 Diseño asistido por computadora
## Diseño asistido por computadora

Mi proceso de aprendizaje de las herramientas digitales para diseño, ha sido autodidacta, mediante la lectura de manuales y ahora es uso de tutoriales web o videos de youtube. A medida que tengo que resolver una situación profundizo en el tema y si no se hacer algo busco cómo se debería hacer.
El software que uso es Adobe Illustrator y Photoshop, pero hace muchos años hacía uso de Corel, esta es la primer aves que utilizo software libre y me parecieron muy similares en herramientas y lógica de uso a lo que conocía.
Para el desafío planteado use Adobe Illustrator, inicie con un boceto y en el programa utilice una lógica similar a un pictograma o icono gráfico



![](../images/proceso02.jpg# center)
![](../images/proceso01.jpg# center)
![](../images/proceso03.jpg# center)

El resultado fianl del icono para el proyecto es el siguinete:

![](../images/Icono_izzi.jpg# center)

 [Archivo Icono_lamp_3d.ai](../files/Icono_lamp_3d.ai)

El trabajo con imágenes raster o de pixels, generalmente lo hago para la presentación de proyectos o como resumen de un proceso de diseño o investigación visual. Si bien el manejo de la herramienta lo considero limitado, dado las posibilidades que tiene el programa. En este caso trabaje con varias layers para componer un imagen que representa la impresión 3D de luminarias inspiradas en la naturaleza.

![](../images/concepto01.jpg# center)


 [Archivo Concepto_01](../files/Concepto_01.psd)

En el proyecto que tengo pensado desarrollar, el uso de estas herramientas, me podría permitir el desarrollo de material para la generación de texturas tridimensionales. El uso de imágenes rasters o pixel, me van a servir como insumo para la herramienta de grasshopper image sampler.
La siguiente imagen se muestra como la creación de texturas se puede transformar en una base para la generación de texturas 2D en vectorial y a partir de ahí generar una estructura tridimensional. Que se podría aplicar a la volumetría de algunos de los proyectos a desarrollar.

![](../images/imagesampler01.jpg# center)


Con respecto al uso de vectoriales en herramientas como Adobe Ilustrator o Indesign, van a servir para la presentación de la propuesta y el desarrollo del material grafico que va acompañar cada una de las piezas.
