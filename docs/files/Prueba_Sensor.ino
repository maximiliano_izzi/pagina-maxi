/*
By: Nathan Seidle SparkFun Electronics  
Library: http://librarymanager/All#SparkFun_SCD30  
*/

#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h" 

SCD30 airSensor;



void setup()  
{
  Wire.begin();
  Serial.begin(9600);
  Serial.println("SCD30 Example");
  airSensor.begin(); //This will cause readings to occur every two seconds
}

void loop()  
{
  if (airSensor.dataAvailable())
  {
    scd30.forced_recalibration_reference = 400
    Serial.print("co2(ppm):");
    Serial.print(airSensor.getCO2());

    Serial.print(" temp(C):");
    Serial.print(airSensor.getTemperature(), 1);

    Serial.print(" humidity(%):");
    Serial.print(airSensor.getHumidity(), 1);

    Serial.println();
  }
  else
    Serial.println("No data");

  delay(1000);
}
