#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h" 

#define espera 60000 // espera de 60 segundos 
SCD30 airSensor;

int retardo=5;          // Tiempo de retardo en milisegundos (Velocidad del Motor)
int dato_rx;            // valor recibido en grados
int numero_pasos = 0;   // Valor en grados donde se encuentra el motor

void setup()  
{
  pinMode(11, OUTPUT);    // Pin 11 conectar a IN4
  pinMode(10, OUTPUT);    // Pin 10 conectar a IN3
  pinMode(9, OUTPUT);     // Pin 9 conectar a IN2
  pinMode(8, OUTPUT);     // Pin 8 conectar a IN1
  
  Wire.begin();
  Serial.begin(9600);
  //Serial.println("SCD30 Example");
  airSensor.begin(); //This will cause readings to occur every two seconds
  
} // Fin del setup

void loop()  
{
  if (airSensor.dataAvailable()) //Si hay medida disponible 
  {
    
    dato_rx = int(airSensor.getCO2()); // Guardo medida CO2 en dato_rx 
    
    Serial.print("co2(ppm):");
    Serial.print(dato_rx);
    Serial.println();
  }
  else{ //si no se imprime "No data"
  
    Serial.println("No data");
  }
  
  dato_rx = (dato_rx * 1.4222222222); // Ajuste de 512 vueltas a los 360 grados
  dato_rx = map(dato_rx,400,6000,-25,125);
  Serial.println();
  Serial.print("Grados:");
  Serial.print(dato_rx);
  Serial.println();
  
   while (dato_rx>numero_pasos){   // Girohacia la izquierda en grados
       paso_izq();
       numero_pasos = numero_pasos + 1;
   }
   
   while (dato_rx<numero_pasos){   // Giro hacia la derecha en grados
        paso_der();
        numero_pasos = numero_pasos -1;
   }
   
  apagado();         // Apagado del Motor para que no se caliente
  
  delay(espera); 
  
} // Fin del Loop 


void paso_der(){         // Pasos a la derecha
 digitalWrite(11, LOW); 
 digitalWrite(10, LOW);  
 digitalWrite(9, HIGH);  
 digitalWrite(8, HIGH);  
   delay(retardo); 
 digitalWrite(11, LOW); 
 digitalWrite(10, HIGH);  
 digitalWrite(9, HIGH);  
 digitalWrite(8, LOW);  
   delay(retardo); 
 digitalWrite(11, HIGH); 
 digitalWrite(10, HIGH);  
 digitalWrite(9, LOW);  
 digitalWrite(8, LOW);  
  delay(retardo); 
 digitalWrite(11, HIGH); 
 digitalWrite(10, LOW);  
 digitalWrite(9, LOW);  
 digitalWrite(8, HIGH);  
  delay(retardo);  
}

void paso_izq() {        // Pasos a la izquierda
 digitalWrite(11, HIGH); 
 digitalWrite(10, HIGH);  
 digitalWrite(9, LOW);  
 digitalWrite(8, LOW);  
  delay(retardo); 
 digitalWrite(11, LOW); 
 digitalWrite(10, HIGH);  
 digitalWrite(9, HIGH);  
 digitalWrite(8, LOW);  
  delay(retardo); 
 digitalWrite(11, LOW); 
 digitalWrite(10, LOW);  
 digitalWrite(9, HIGH);  
 digitalWrite(8, HIGH);  
  delay(retardo); 
 digitalWrite(11, HIGH); 
 digitalWrite(10, LOW);  
 digitalWrite(9, LOW);  
 digitalWrite(8, HIGH);  
  delay(retardo); 
}
        
void apagado() {         // Apagado del Motor
 digitalWrite(11, LOW); 
 digitalWrite(10, LOW);  
 digitalWrite(9, LOW);  
 digitalWrite(8, LOW);  
 }
