# Sobre mi

![](../images/maximilianoizzi.jpg)

Master en Gestión y Diseño para Sistemas de Productos Destinados al Espacio Doméstico, Universitádegli Studi di Roma – La Sapienza. Postgrado en Gestión Empresarial y Postgrado en Muebles, Centro de Diseño Industrial. Diseñador Industrial, Consultor en desarrollo de nuevos productos y ergonomía laboral. Profesor de Diseño Industrial y Ergonomía, Facultad de Comunicación y Diseño y Ergonomía en la Facultad de Arquitectura de la Universidad ORT Uruguay.

El interés de participar de la especialización, parte del desafió de incorporar las nuevas tecnologías productivas,  en el diseño y desarrollo de nuevos productos que se puedan incorporar las nuevos ambientes de uso.

Parte de los trabajos realizados se pueden ver en la web de mi estudio **[my website](http://www.izzidesign.uy/)**
