---
hide:
    - toc
---

# MP 01 Fabricación digital e Innovación
## Introducción a la Fabricación digital e Innovación

Mi acercamiento a la producción digital se inicio hace más de 20 años, al entrar en contacto con las maquinas de CNC usadas en las carpinterías, para el mecanizado de piezas. A partir de esta tecnología las posibilidades de diseño y producción se vieron ampliadas. Creando un interés por comprender en mayor profundidad su funcionamiento, para explotar al máximo sus capacidades. Una de mis primeras experiencias fue el diseño y fabricación del mostrador para el noticiero de Canal 5. Realizado en de forma simultanea en un taller de carpintería con una máquina CNC y la parte de vidrio en una vidriería con una tecnología similar, permitiendo reducir tiempos y logrando dos piezas que encastraron perfectamente.

![](../images/sodre.jpg# center)

En estos años he visto como la accesibilidad a estas nuevas tecnologías ha aumentado, incorporándose en diferentes sectores productivos. Facilitando y mejorando la calidad de la producción local. Surgiendo la oportunidad de vincular los procesos de diseño, fabricación, comercialización y distribución para generar nuevas ofertas de productos y servicios, la fabricación digital se ha perfilado como una de las tecnologías más revolucionarias que busca transformar diversos ámbitos en el mundo de la manufactura. Así, muchas empresas trabajan a diario para incorporarla en sus diversas áreas de negocio y así sumarse en el tren de la innovación disruptiva.

La fabricación digital busca reducir el ciclo de desarrollo de productos desde la idea hasta el prototipo y crear nuevos diseños de forma rápida y económica. Este proceso también les permite a las empresas flexibilizar su producción y cambiar diseños y productos a disposición del cliente. Con ello, abren oportunidades para el desarrollo local, accediendo a herramientas de diseño que antes sólo podían disponer las grandes firmas.

Por ejemplo, [OpenDesk](https://www.opendesk.cc/) {:target="_blank"} es un sitio web que ofrece diseños de muebles a partir de la impresión 3D, los cuales pueden ser fabricados localmente en cualquier lugar del mundo. Por su parte, [Shapeways](https://www.shapeways.com/) {:target="_blank"} es una empresa Holandadese con sede en Nueva York que les ofrece a los usuarios la posibilidad de imprimir en 3D sus diseños creativos y personalizados.

![](../images/opendesk.jpg# center)

Según [Deloitte](https://www2.deloitte.com/us/en/insights/topics/digital-transformation.html) {:target="_blank"}, los casos para usar iniciativas de fabricación
digital incluyen:

• Mantenimiento preventivo para evitar fallas costosas en maquinaria
• Detección de calidad para monitorear y probar equipos y productos en tiempo real con análisis visual
• Eficiencia predictiva para evitar costosos retrasos
• Monitoreo de temperatura para realizar los ajustes necesarios en
herramientas de corte para temperaturas más altas
• Optimización de los puestos de trabajo en beneficio de toda la línea
de producción
• Transporte inteligente para automatizar el movimiento del material
• Colaboración de ingeniería y gemelos digitales para la creación rápida de
prototipos, configuración de celda de producción virtual y modelado
de productos digitales.
• Seguimiento de activos en tiempo real
• La capacidad de ajustar la producción para satisfacer las
necesidades cambiantes de los clientes y los pedidos urgentes


Para la propuesta de proyecto innovador, se presentaron en Miro tres ideas. La primera, del diseño y desarrollo de luminarias impresas 3D, con referencias morfológicas al crecimiento natural de la naturaleza y un sistema de sensores cinéticos para que responda a los eventos del ambiente, La segunda un sistema de entrenamiento de reflejos con sensores y luces y por último un Gallinero Smart, para colocar en espacios urbanos.

En la siguiente imagen se puede ver lo presentado en el Miro

![](../images/miro_proyecto_innovador.jpg# center)
