---
hide:
    - toc
---

# MP 03 Transformación Digital/Materiales&FD

Generación de Biomateriales



Los objetivos de la economía circular, son eliminar o minimizar la generación de residuos y contaminación en todas las fases de fabricación, distribución, comercialización y desecho de los productos. Pasando de una lógica lineal, donde se extrae, procesa, usa y desecha a una circular donde los productos después de su uso, vuelven a formar parte, por diferentes medios del proceso de producción. Con la finalidad de ayudar a regenerar la naturaleza.

![](../images/Diagrama-sistémico-ES.png# center)

Una forma muy facil de compreder y visaulizar esta nueva forma de desarrollo, la podemos ver en el esquema de la economía circular hecho por [Ellen Macarthur Foundation](https://ellenmacarthurfoundation.org/circular-economy-diagram)

<iframe width="560" height="315" src="https://www.youtube.com/embed/NBEvJwTxs4w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Sitios WEB para explorar la fabricación de Bio - materiales

[Materiability](https://materiability.com/) - Excelente sitio con muchos materiales y recetas muyu claras


![](../images/materaility.jpg# center)


[Bioplastic Cook Book](https://issuu.com/nat_arc/docs/bioplastic_cook_book_3): Es un libro de recetas de Biomateriales al mejor estilo libro de comidas


Muy recomendable es el sitio del [FABLAB de BCN](https://fablabbcn.org/blog/emergent-ideas/biomaterials-101) ya que tiene recetas, videos e imagines que ayudan a entender el proceso y motivan a experimentar.

Recetas y pruebas realizadas:

Componentes básicos de un biomaterial

Biopolímeros: agar agar, gelatinas, resina.
Plastificantes: glicerina, cera.
Aditivos: residuos orgánicos aportan la resistencia, color, fibras
Disolventes: agua (para gelatina y agar agar), alcohol (para resina). Desaparecen por evaporación.
Tips

Carozos: como regla general: hervir + moler + secar
Según el granulado del aditivo es mejor la gelatina (mayor tamaño) o el agar agar (menor tamaño)
Moldes: para agar agar: liso (acrílico), para gelatina: rugosa (textil poroso), para resina: desmoldar sobre papel de horno para que evitar que se pegue.
Se pueden agregar aceites esenciales o vinagre a las mezclas para evitar el crecimiento de hongos.

![](../images/pract4.jpg# center)

![](../images/pract1.jpg# center)

![](../images/pract3.jpg# center)

![](../images/pract2.jpg# center)

![](../images/pract6.jpg# center)
