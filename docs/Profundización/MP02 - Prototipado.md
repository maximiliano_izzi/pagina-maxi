---
hide:
    - toc
---

# MP 02 Prototipado

Explorar Futuros emergentes


El inicio de este modulo tuvimos una exposición de Mariana Quintero donde destaco:
-	La velocidad del cambio ha aumentado
-	Las estructuras se deben adaptar rápidamente para que los cambios no los pasen por arriba
-	Las modelos tradicionales de extracción, fabricación, distribución, consumo y desechar, es insostenible y surgen nuevos modelos de negocios que rompen con este paradigma.

Para poder anticiparse a estos cambios y comprender la complejidad del entorno actla se debe tener una visión holistica. La empresa TrendWaching´s nos plantea 29 megatendencia globales que nos pueden ayudar a entender el entrono, donde se producen los cambios.

![](../images/mega_trends.jpg# center)

Esta información esta accesible en miro, mediante un canvas para el análisis de tendecias

Según [TrendWaching´s](https://www.trendwatching.com/toolbox/consumer-trend-canvas) {:target="_blank"},

• Mantenimiento preventivo para evitar fallas costosas en maquinaria
• Detección de calidad para monitorear y probar equipos y productos en tiempo real con análisis visual
• Eficiencia predictiva para evitar costosos retrasos
• Monitoreo de temperatura para realizar los ajustes necesarios en
herramientas de corte para temperaturas más altas
• Optimización de los puestos de trabajo en beneficio de toda la línea
de producción
• Transporte inteligente para automatizar el movimiento del material
• Colaboración de ingeniería y gemelos digitales para la creación rápida de
prototipos, configuración de celda de producción virtual y modelado
de productos digitales.
• Seguimiento de activos en tiempo real
• La capacidad de ajustar la producción para satisfacer las
necesidades cambiantes de los clientes y los pedidos urgentes


Para la propuesta de proyecto innovador, se presentaron en Miro tres ideas. La primera, del diseño y desarrollo de luminarias impresas 3D, con referencias morfológicas al crecimiento natural de la naturaleza y un sistema de sensores cinéticos para que responda a los eventos del ambiente, La segunda un sistema de entrenamiento de reflejos con sensores y luces y por último un Gallinero Smart, para colocar en espacios urbanos.

En la siguiente imagen se puede ver lo presentado en el Miro

![](../images/miro_proyecto_innovador.jpg# center)

Dinámica para explorar Futuros emergentes

La propuesta es identificar nuevas nexos, enfoques, miradas, modificaciones, énfasis, etc. de nuestra idea de proyecto, pero ahora a partir de 3 insumos ( Señales Débiles, Conceptos detonantes y Áreas de oportunidad).

Se llego al siguiente resultado

![](../images/prototipo_miro.jpg# center)

En este panel de aplican Atlas de Señales Débiles

Son 25 indicadores o signos emergentes que brindan pistas y oportunidades para proyectar en relación a ellos, porque dan claves del futuro que se vislumbra, o que se esta desarrollando.

![](../images/atlas.jpg# center)

A estos indicadores, se le suman 90 tópicos o ideas emergentes, con el objetivo de generar o ampliar las posibilidades del proyecto.

![](../images/atlas.jpg# center)

Para finalizar se uso las áreas de oportunidad, son  herramientas, infraestructuras o tecnologías actuales con las que se puede asistir un proceso de prototipado o ideación de un futuro emergente.

![](../images/areas.jpg# center)

PROTOTIPADO DE LA MARCA

¿Quién es tú audiencia?

Se debe definir claramente a quienes esta destinado el proyecto, no solamente su edad, sexo. Se debe hacer énfasis en sus expectativas, deseos, educación y necesidades. Definir el estilo de vida

¿Cómo será el proyecto?

Describirlo como si fuer aun tweet, una descripción concreta y realista. Evitar el uso de tecnicismos o terminología compleja. Mencionar lo hace ahora el proyecto y lo que debería o te gustaría.
Buscar formas visuales

Crear una identidad del Proyecto para facilitar su recordación y que se asocie rápidamente al verlo, con nuestra marca o nombre.
El uso de una paleta de colores adecuada y una tipografía asociada al estilo del proyecto. Regla del uso de colores: 60% el color primario, 30% el secundario y 10% el color para acento.

Formar una comunidad

Abrir el proyecto: mostrar el progreso del trabajo. Se debería crear y compartir objetivos con otros, mostrar el progreso. Con sus éxitos y errores, ya que todo refleja honestidad. También se puede mostrar qué proyectos resultan inspiradores y te sirven de guía o motivación. En caso de tener un equipo, mostrar los integrantes y cuáles son sus aportes Identificar potenciales líneas de contenido en base a lo que ya estoy haciendo en la vida diaria. Elejir el formato: blog, video, texto, imágenes, etc.)


Trabajo de la MARCA

![](../images/marca_01.jpg# center)

![](../images/marca_02.jpg# center)

![](../images/marca_03.jpg# center)
