---
hide:
    - toc
---

# MI 02 - Diseño COMPUTACIONAL
## Diseño COMPUTACIONAL

El diseño computacional, tiene como base la parametrización de la creación de objetos, que permite mediante la variación de algunas variables, modificar la geometría, aspecto y terminación de la pieza. Esto posibilita la generación de diferentes alternativas y probar el cambio de detalles.
El uso de este proceso, posibilita mediante el uso de plug-ins crear soluciones que de otra forma podrían llevar mucho tiempo o ser casi imposible de realizar.
En el caso de proyecto que estoy desarrollando, me interesa mucho esta herramienta, ya que me permitirá genera geometrías cercanas a las naturales y a partir de una programación, crear variaciones en tamaño y proporciones.
Para el diseño computacional, hice uso de Rhinoceros + Grasshopper, programa que ya había empezado a usar y con el taller de Arturo de la Fuente, me sirvió para ampliar su practica y profundizar su conocimiento.
Los dos modelos que presente los realice en grasshopper, ya que parte de la experimentación de la impresión 3D, era ver como quedan las texturas tridimensionales y que grado de traslucides se logra con la impresión 3D.
En la siguiente imagen se puede ver la programación realizada para la primera prueba impresión realizada.

![](../images/cactus01.jpg# center)

El resultado logrado es sutil a nivel tactil y visual. En base a los resultados se podría aumentar la altura del relieve y bajar el espesor de la pared, para facilitar el pasaje de la luz y que la textura se pueda ver mejor.
La programación de Grasshopper queda disponible en este link

 [Archivo Prueba_01.gh](../files/Puerba_01.gh)

 Se esperaba logar una textura más acentuada, tanto visaul como táctil. En la siguinete foto de la impresión 3D, se puede ver que este no fue el resultado.

 ![](../images/impresion01.jpg# center)


En la segunda prueba de modelado, paso algo similiar, solo que más acentuado debido al espesor que quedo en la pared de la pieza. Se debería tener una fluctuaión de espesores entre 3mm y 0.6mm. Como alternativa podría tenes espacios abiertos en el lateral la pieza.

![](../images/cactus02.jpg# center)

 [Archivo Prueba_02.gh](../files/Puerba_02.gh)

El resultado esperdo lo podemos ver en el render preview

![](../images/cactus_02.jpg# center)

En este caso el resultado logrado fue más interesante y se llego a generar una textura visual y tactil más percibible. Sumando el efecto al pasar la luz.

![](../images/impresion02.jpg# center)

Grasshopper es una herramienta que nos permite hacer visible una programación, y con esta programación resolver un diseño de una forma iterativa y parametrizada. Generalmente es muy usado para aplicar texturas en objetos. Esta excelente herraienta se puede complementar con plugin. Alguno de los más recomendados son:

Parakeet: generación de patrones algorítmicos

Panelingtools: racionalizar la geometría compleja

Crane: diseño de origamis.

Ameba: analiza estructuras y las optimiza, sacándole material sin debilitarlas.

PufferFish: coloca cualquier objeto en una superficie.

OpenNest: distribuye piezas 2d sobre placas.(útil para cnc o láser).

Galapagos: busca la menor caja contenedora para una geometría.

Biomorpher: analiza variantes en formas para cumplir ciertos requerimientos.

Kangaroo: calcula y hace simulaciones, movimientos.

Voronoi: hace regiones patrones ( piel jirafa).

FlexHopper: calcula y simula movimientos con partículas.


En su gran mayoría estos plug-in se pueden desacargar de la pagina web [FOOD4RHINO](https://www.food4rhino.com/)

![](../images/food4rhino.jpg# center)

Para poder descargarlos, se tiene que crear un usuario en food4rhino, esto es gratuito. a partir de esto se selecciona el plug-in que se necesita y se desacarga. para instalarlo, primero se tiene que desbloquear el archivo, en propiedades en el archivo. Además se debe guardar en la carpeta de componentes en grasshopper.
