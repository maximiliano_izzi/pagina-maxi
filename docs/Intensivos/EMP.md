---
hide:
    - toc
---

# MI01 - Emprendimientos
## Modulo Emprendimientos

En esta semana del Modulo de Emprendimientos, tuve la posibilidad de tener una revisión de temas que manejaba y verlos desde otra perspectiva. Permitiéndome ampliar su conocimiento y experiencia. Se pudo estudiar la realziación del Canvas de Modelo de Negocio, el cual me sirvio para visulizar la infrastrucutura necesaria para ofrecer la propuesta de valor.


![](../images/canvaizzi.jpg# center)

Parte del material exigido para el cierre del taller fue un diagrama de Gantt con el listado de actividades y asignación de tiempos para cada una de ellas

![](../images/gantt01.jpg# center)

A partir del desarrollo de los diferentes talleres, de impresión 3D, modelado computacional y arduino. Se puede constatar que los tiempos asignados para algunas delas actividades han quedado corta de tiempo y se debería realizar una revisión del diagrma de Gantt presentado.

La devolución del la propuesta presentada, por parte de Sergio Delgado, fue la siguiente:

Gracias Maximiliano por tu trabajo. Ambas herramientas están logradas, en el sentido de sentido y aporte para la acción. En el caso del canvas, cuidado con el cuadrante de relación-con-el-cliente que parece el contenido actual se confunde con canales (en ese cuadrante se detalla el encuadre de llevar adelante la relación, en un continua que los extremos son: hiper-personalización -------------------------------hiper-automatización). Acordate de la táctica de Shopify versus Amazon para ganarle mercado que mencionó tefa en la sesión del día jueves. Para ahora y también para después en el caso de que quieras seguir enriqueciendo este lienzo, te recomiendo la guía:
<https://drive.google.com/file/d/16RCPU2NCf82nclYwCDsiMmR7rqK-giwe/view?usp=sharing>
