---
hide:
    - toc
---

# TIP - Taller Integrador Presencial


La propuesta del Taller fue Hackear una impresora Anet A8, para realizar impresión en cerámica.
Para hacer esto trabajamos en equipo, en mi caso estuve con Rodrigo deArmas.

Primero nos enfocamos en el desmontaje de la Anet A8

![](../images/anet08.jpg# center)

Una vez desmontado el extrusor y el fan de la impresora, teníamos que conectar la placa la computadora para actualizar su programación, ya que no se iba a necesitar controlar la temperatura del extrusor. Si esto no se modificaba, la impresora no funcionaría, ya que marcaría un error al no leer la temperatura. Cómo no se pudo conectar la placa con la Pc, ya que no se reconocía. Tuvimos que dejar el extrusor a un costado y montar el dispositivo para extrudir cerámica.

![](../images/anet08.jpg# center)

Colcado el sistema de engranejes para usar el motor de la extrusora de FDM, para empugar una embolo dentro de una jeringa. Empezamos hacer pruebas con la pasta cerámica.

![](../images/pasta.jpg# center)

Al realizar las pruebas nos pudimos dar cuentas que teníamos muchas variables para controlar que afectaban el resultado final.
Una de ellas tenía que ver con el slicer y la programación, siendo muy importantes la altura de capa de impresión y velocidad de impresión. También el poner impresión en espiral ayudaría a que la boca de salida de material no pase por arriba de la pieza.
Con respecto al material lo más difícil de lograr fue una viscosidad que fuera fácil de extruir, pero no excesiva para que al salir se desplomará.
La experiencia nos dejó con ganas de profundizar en el tema, pero a partir de un sistema de extrusión más controlado que el usado.
