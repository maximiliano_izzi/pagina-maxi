---
hide:
    - toc
---

# MI 02 - Innovación Abierta


La Innovación Abierta se puede definir como todos los flujos internos y externos de conocimiento que pueden acelerar la innovación interna y expandir los mercados. Es un cambio de paradigma en el proceso de innovacioón donde se pasa de estar en estrucuturas cerradas a plataformas abiertas de conocimiento, para acelerar el proceso y mejorar los resultados de la innovación.

¿Cómo puedes incorporar técnicas de innovación abierta en tu estrategia?
Para respaldar el despliegue de la innovación abierta, necesita las herramientas y los procesos adecuados.

Unilever y GSK Consumer Healthcare refinaron el proceso "Querer, encontrar, obtener, administrar" (WFGM) que ayuda a las organizaciones a dividir su innovación abierta en cuatro fases distintas:

Deseo: en esta etapa del proceso, las organizaciones deben identificar las brechas en su innovación y preguntarse si es mejor llenarlas interna o externamente.

Encontrar: la segunda etapa se trata de decidir qué grupos externos ayudarán a las organizaciones a desarrollar las capacidades para cumplir con los deseos identificados en la primera etapa.

Obtener: aquí las organizaciones identifican los procesos que utilizarán para planificar y estructurar la captura de ideas y conocimientos de estas fuentes externas.

Administrar: la última etapa analiza las herramientas y las métricas que las organizaciones utilizarán para rastrear y monitorear sus relaciones e ideas con las partes interesadas externas.

Este marco es un punto de partida útil para que las organizaciones planifiquen la innovación abierta.

El árticulo completo se puede  encontrar en el siguiente link [Ideadrop.co](https://ideadrop.co/innovation-strategy/what-is-open-innovation/)

En el área de diseño y llevado de la mano de los procesos de fabracición digital, ha surguido el open design.

![](../images/51UifA-YU4L.jpg# center)

"El diseño está viviendo una revolución. La tecnología está capacitando a más personas para crear y difundir diseños, y los profesionales y entusiastas la están utilizando para compartir su trabajo con el mundo. El diseño abierto está cambiando todo, desde los muebles hasta la forma en que los diseñadores se ganan la vida."

[OpenDesign](http://opendesignnow.org/index.html)

De su apliación se pueden ver varios ejemplos, pero los más difundidos, son aquellos sobre mobiliario. Uno de sus mayores referentes es Ronen Kadushin, que centro su trabajo de Maestría en el Open designa

![](../images/FIT_Italic_shelf_half+front_DSC0692_V4.jpg# center)

todo su trabajo puede ser explorado y desacargado de su pagina web

![](../images/51UifA-YU4L.jpg# center)


[Ronen Kadushin](https://www.ronen-kadushin.com/)
