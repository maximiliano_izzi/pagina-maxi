---
hide:
    - toc
---

# Propuesta final
Proyecto Luminaria Flos - CO2
## Luminaria con sensor de CO2


Proyecto de luminaria que combina el diseño paramétrico con los procesos de fabricación digitales, para lograr una luminaria que cambia su forma, dependiendo del nivel de CO2 en el ambiente

![](../images/Asset01.png# center)

![](../images/Asset04.png# center)

![](../images/Asset05.png# center)

![](../images/Asset06.png# center)

![](../images/Asset07.png# center)

![](../images/Asset08.png# center)

![](../images/Asset09.png# center)

![](../images/Asset10.png# center)

![](../images/Asset11.png# center)

![](../images/Asset12png# center)

![](../images/Asset13.png# center)

![](../images/Asset14.png# center)

![](../images/Asset15.png# center)

![](../images/Asset16.png# center)

![](../images/Asset03.png# center)

<iframe width="560" height="315" src="https://www.youtube.com/embed/uscCz1Q7s4M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Documentación del proyecto:

Programación de los petalos en Grasshopper
[Archivo leaf_01.gh](../files/leaf_01.gh)

Modela 3D de la superficie para el petalo en Rhinoceros 3D
[Archivo Petalo_superficie.3dm](../files/Petalo_superficie.3dm)

Modela 3D de la base y mecanismos en Rhinoceros 3D
[Archivo Base_movimiento.3dm](../files/Base_movimiento.3dm)

Programación de Arduino Lampara Flos CO2
[Archivo Lampara_CO2.ino](../files/Lampara_CO2.ino)
