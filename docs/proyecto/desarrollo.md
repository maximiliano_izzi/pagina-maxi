---
hide:
    - toc
---

# Desarrollo

El inicio del proyecto surge a partir de inquietudes personales para incorporar la geometría de la naturaleza y aplicarla a la generación de formas innovadoras con el uso del software paramétrico. El uso de la luz y la creación de entornos singulares y transformables, son la premisa que guía el proyecto. Que proponen una nueva tipología de iluminación ambiental, que interactúa con el ambiente donde se coloca.

En este proceso. se debe comprender diferentes conceptos sobre la geometría de la naturaleza, para poder trasladarlos y aplicarlos al diseño de objetos.

¿Qué es la secuencia de Fibonacci?

La secuencia de Fibonacci es una de las fórmulas más famosas de las matemáticas. Cada número de la secuencia es la suma de los dos números que lo preceden. Entonces, la secuencia es: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, etc. Se le ha llamado "código secreto de la naturaleza" y "regla universal de la naturaleza". Un ejemplo perfecto de esto es la concha de nautilus, cuyas cámaras se adhieren casi perfectamente a la espiral logarítmica de la secuencia de Fibonacci.

![](../images/secuenciafibonacci.jpg# center)

Este famoso patrón aparece en todas partes de la naturaleza, incluidas flores, piñas, huracanes e incluso enormes galaxias espirales en el espacio. Pero la secuencia de Fibonacci no se limita a la naturaleza. En diseño, nos referimos a ella como la proporción áurea. Se puede aplicar a todo, desde diseño de logotipos, mobiliario y edificios.

![](../images/nautilussheel.jpg# center)

¿Qué es la proporción áurea?
La proporción áurea es un concepto de diseño basado en el uso de la secuencia de Fibonacci para crear proporciones visualmente atractivas en el arte, la arquitectura y el diseño gráfico. La proporción, el tamaño y la ubicación de un elemento en comparación con otro crea una sensación de armonía que atrae a nuestra mente subconsciente.

![](../images/golden-ratio-logos.jpg# center)

Al diseñar, no existen reglas a la hora de crear una estética o un diseño efectivo para un proyecto. Si bien encontramos que la proporción áurea funciona perfectamente en la aplicación, mientras que otras veces se nos ocurre una idea que no necesariamente sigue las reglas.
El uso de la proporción aurea, es discutida por algunos diseñadores, pero hay evidencia, no solo en la naturaleza sino en el diseño creado, que demuestra que la proporción aurea es una herramienta sólida para proyectar la geometría de un objeto. En general, es una forma interesante de ver un diseño a través de una lente matemática y creo que es fascinante identificar dónde y cómo se usa en el mundo que nos rodea.

![](../images/alto_aureo.jpg# center)

Al investigar la información sobre la Serie de Fibonacci y la proporción aurea encontré que esta disponible en varios reservorios web, la información necesaria para hacer un compás aureo, en la versión corte laser o impreso 3D

Dejo accesible los links y la imagen de cada uno de ellos, en un futuro pienso producir uno de estos para evaluar su funcionamiento y verificar los diseños realizados

Al investigar la información sobre la Serie de Fibonacci y la proporción aurea encontré que esta disponible en varios reservorios web, la información necesaria para hacer un compás áureo, en la versión corte laser o impreso 3D

En la web de [Inustructables ](https://instructables.com/Laser-Cut-Fibonacci-Gauge/) esta la información para realizar un compas con corte laser.

![](../images/lasertcutfibonacci.jpg# center)

También podemos encotrar otra vesrión en [Thingdiverse ](https://thingiverse.com/thing:895616) esta vez es una versión impresa, pero queda claro que es facil hacer un compas áureo que nos ayude a verificar nuestros diseños y a encontrar este tipo de relaciones en la naturaleza.

![](../images/compaimpreso3d.jpg# center)

Si el tema les resulta tan interesante cómo a mí, agrego un video de youtube donde se explica de forma más clara la serie de Fibonacci, su presencia en la naturaleza y posibles aplicaciones.

<iframe width="1180" height="664" src="https://www.youtube.com/embed/1Jj-sJ78O6M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
