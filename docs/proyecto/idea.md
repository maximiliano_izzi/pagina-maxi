---
hide:
    - toc
---


# Idea y Concepto

El proyecto tiene como objetivo diseñar y desarrollar Luminarias impresas 3D, inspiradas en la naturaleza, para crear espacios únicos y sorprendentes, incorporando un sistema de iluminación cinético que interactúa con los sonidos del entorno o el movimiento de las personas. También se podrá evaluar la posibilidad de una conectividad wifi o bluetooth para su personalización y ajustes.
Diseños personalización a través de un modelado 3D paramétrico. Archivo 3D de la lampara para deslocalización de la fabricación Productos sustentables y amigables con el medio ambiente, producidos en PLA, derivado del Maíz y reducción de la huella de carbono al deslocalizar la producción y minimizar los traslados.

La motivación para iniciar un proyecto de estas características, es juntar varías áreas del conocimiento y habilidades que debe tener un diseñador para afrontar los desafíos de la actividad proyectual en el S XXI. El poder realizar un producto que involucre la producción por medios digitales de impresión y mecanizado CNC, me acerca a la programación enfocada a la producción y considerar las restricciones que tiene ambos procesos en el diseño de una pieza.

![](../images/smart_product.jpg# center)

La inclusión en el producto de una capacidad reactiva sucesos del entorno y que pueda interactuar con el usuario. Me permite desarrollar un proyecto personal, que contemple elementos para proyectar productos Smart, alto potencial de vincularse con otros dispositivos cómo celulares o computadoras. Así con las plantas pueden reflejar la calidad del aire o cantidad de luz solar que reciben, el producto podría representar a través de la luz, forma o color información para las personas que están en el ambiente.

Una forma de representar los desafíos del proyecto sería considerar los Diez principios para el diseño en la era de la AI, escritos por el diseñador [Yves Behar](https://fuseproject.com/){:target="_blank"}. Creador del estudio Fuseproject


1. EL DISEÑO RESUELVE UN IMPORTANTE PROBLEMA HUMANO.
2. EL DISEÑO ES ESPECÍFICO DEL CONTEXTO (NO SIGUE CLICHES HISTÓRICOS).
3. EL DISEÑO MEJORA LA CAPACIDAD HUMANA (SIN REEMPLAZAR A LA HUMANA).
4. EL BUEN DISEÑO FUNCIONA PARA TODOS, TODOS LOS DÍAS.
5. LA BUENA TECNOLOGÍA Y EL DISEÑO SON DISCRETOS.
6. EL BUEN DISEÑO ES UNA PLATAFORMA QUE CRECE CON NECESIDADES Y OPORTUNIDADES.
7. EL BUEN DISEÑO OFRECE PRODUCTOS Y SERVICIOS QUE CONSTRUYEN RELACIONES A LARGO PLAZO (PERO NO CREAN DEPENDENCIA EMOCIONAL).
8. EL BUEN DISEÑO DE TECNOLOGÍA APRENDE Y PREDICE EL COMPORTAMIENTO HUMANO.
9. EL BUEN DISEÑO ACELERA NUEVAS IDEAS
10. EL BUEN DISEÑO ELIMINA LA COMPLEJIDAD DE LA VIDA.
