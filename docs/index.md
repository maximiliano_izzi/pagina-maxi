## Welcome

![](../images/perezoso.jpg)

Hola, desde Montevideo Uruguay, trabajo como diseñador industrial, y además soy docente en la Universidad ORT. Estas dos actividades que se complementan, me motivan e incentivan a una formación constante para poder ofrecer a mi clientes y estudiantes propuestas actuales y motivadoras.
La incorporación de tecnologías en los productos y la digitalización de su producción son dos desafíos que quiero abordar con la realización de esta especialización y en  este lugar voy a mostrar los resultados, aprendizaje a modo de bitácora de viaje.
